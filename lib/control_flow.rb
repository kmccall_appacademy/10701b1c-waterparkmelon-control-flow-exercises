# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  str_arr = str.chars
  str_arr.reject {|ch| ch == ch.downcase}.join
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  n = str.length
  if n%2 == 0
    str[(n/2 - 1)..n/2]
  else
    str[n/2]
  end
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  vowel_count = 0
  str.downcase.each_char do |ch|
    vowel_count += 1 if VOWELS.include?(ch)
  end
  vowel_count
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  start = 2
  product = 1
  until start > num
    product*=start
    start += 1
  end
  product
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  str = ""
  arr.each_with_index do |word, i|
    if i == 0
      str << "#{word}"
    else
      str << "#{separator}#{word}"
    end
  end
  str
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  string =
  str.chars.map.with_index do |ch, i|
    if i%2 == 0
      ch = ch.downcase
    else
      ch = ch.upcase
    end
  end

  string.join
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  reversed =
  str.split.map do |word|
    if word.length >= 5
      word.reverse
    else
      word
    end
  end
  reversed.join(" ")
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  start = 1
  array = []
  until start > n
    if start%15 == 0
      array << "fizzbuzz"
    elsif start%3 == 0
      array << "fizz"
    elsif start%5 == 0
      array << "buzz"
    else
      array << start
    end
    start += 1
  end
  array
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  reversed = []
  arr.each do |el|
    reversed.unshift(el)
  end
  reversed
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  if num < 2
    false
  else
    [*2..(num/2)].none? {|n| num%n == 0}
  end
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  factor = []
  (1..num).each do |n|
    if num%n == 0
      factor << n
    end
  end
  factor
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  factors(num).select {|factor| prime?(factor)}
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).length
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  evens = []
  odds = []
  arr.each do |el|
    if el%2 == 0
      evens << el
    else
      odds << el
    end
  end

  if evens.length == 1
    evens.first
  else
    odds.first
  end
end
